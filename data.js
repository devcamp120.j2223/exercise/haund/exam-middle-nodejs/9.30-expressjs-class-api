
class User{
    constructor(paramID, paramName, paramPosition, paramOffice, paramAge, paramStartDate){
        this.id = paramID;
        this.name = paramName;
        this.position = paramPosition;
        this.office = paramOffice;
        this.age = paramAge;
        this.date = paramStartDate;
    }
    // checkAge(paramAge){
    //     return this.age;
    // }
}
let userListClass = [];

let user1 = new User(1, "Airi Satou", "Accountant", "Tokyo", 33, "2008/11/28");

let user2 = new User(2, "Angelica Ramos", "Chief Executive Officer (CEO)", "London", 47, "2009/10/09");

let user3 = new User(3, "Ashton Cox", "Junior Technical Author", "San Francisco", 66, "2009/01/12");

let user4 = new User(4, "Bradley Greer", "Software Engineer", "London", 41, "2012/10/13");

let user5 = new User(5, "Brenden Wagner", "Software Engineer", "San Francisco", 28, "2011/06/07");

let user6 = new User(6, "Brielle Williamson", "Integration Specialist", "New York", 61, "2012/12/02");

let user7 = new User(7, "Bruno Nash", "Software Engineer", "London", 38, "2011/05/03");

let user8 = new User(8, "Caesar Vance", "Pre-Sales Support", "New York", 21, "2011/12/12");

let user9 = new User(9, "Cara Stevens", "Sales Assistant", "New York", 46, "2011/12/06");

let user10 = new User(10, "Cedric Kelly", "Senior Javascript Developer", "Edinburgh", 22 , "2012/03/29");

userListClass.push(user1, user2, user3, user4, user5, user6, user7, user8, user9, user10);

module.exports = {
    userListClass
}