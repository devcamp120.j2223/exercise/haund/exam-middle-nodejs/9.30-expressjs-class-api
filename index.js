
const { response } = require("express");
const express = require("express");
const { request } = require("http");

const app = express();

const port = 8000;

app.use(express.json());

app.use(express.urlencoded({
    extended: true
}))

const routerEmployee = require("./employeeRouter")

// const userListClass = require("./data");

app.get("/", (request, response) =>{
    response.json({
        message: `Hello devcamp, this is R9.30 task`
    })
})

// app.get("/users", (request, response) =>{
//     response.json({
//         "users": userListClass
//     })
// })

app.use("/", routerEmployee);

app.listen(port, ()=>{
    console.log(`App is running on port ${port}`)
})