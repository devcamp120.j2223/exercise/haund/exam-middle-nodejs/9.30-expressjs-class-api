
const { response } = require("express");
const express = require("express");
const { request } = require("http");

const {userListClass} = require("./data");

const { printEmployeeUrlMiddleware } = require("./employeeMiddleware");

const router = express.Router();

router.get("/users", printEmployeeUrlMiddleware, (request, response) =>{

    let age = request.query.age;
    // var test = userListClass.length;
    if(age){
        let userReponseList = [];
        for(let index = 0; index < userListClass.length; index++){

            let userElement = userListClass[index];

            if(userElement.age >= age){ 
                userReponseList.push(userElement);
            }
        };
        response.status(200).json({
            users: userReponseList
        })
    }else{
        response.status(200).json({
            users: userListClass
        })
    }
})

router.get("/users/:userId", printEmployeeUrlMiddleware, (request, response) =>{
    let userId = request.params.userId;
    userId = parseInt(userId);
        let userResponseParam = null;
        for(let index = 0; index < userListClass.length; index++){
            let userElement = userListClass[index];
            if(userId == userElement.id){
                userResponseParam = userElement;
                response.status(200).json({
                    user: userResponseParam
                })
                break;
            }
        };
        response.status(400).json({
            message: "400 Bad Request"
        })
})

router.post("/users", printEmployeeUrlMiddleware, (request, response) =>{
    response.status(201).json({
        message: "POST METHOD"
    })
})

router.put("/users/:userId", printEmployeeUrlMiddleware, (request, response) =>{
    let userId = request.params.userId;

    response.status(200).json({
        message: "PUT METHOD BY ID: " + userId
    })
})


router.delete("/users/:userId", printEmployeeUrlMiddleware, (request, response) =>{
    let userId = request.params.userId;
    response.status(204).json({
        message: "PUT METHOD BY ID: " + userId
    })
})

module.exports = router;